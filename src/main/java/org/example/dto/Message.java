package org.example.dto;

public class Message {
    private Command command;
    private Data data;

    public Command getCommand() {
        return command;
    }
    
    public Data getData() {
        return data;
    }

}
