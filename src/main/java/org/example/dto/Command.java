package org.example.dto;

public enum Command {
    SENDMESSAGE,
    LOGIN,
    LOGOUT,
    CREATECHAT,
    REMOVECHAT,
    REGISTERUSER,
    STARTGAME,
    STOPGAME,
    CURRENT_USER,
    USERS,
    JOINROOM,
    ROOMS;

}
