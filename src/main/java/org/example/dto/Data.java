package org.example.dto;

import java.util.Date;

public class Data {

    //registration command
    private String message;
    private String userId;
    private String roomId;
    private String date;

    public String getMessage() {
        return message;
    }

    public String getUserId() {
        return userId;
    }

    public String getRoomId() {
        return roomId;
    }

    public String getDate() {
        return date;
    }
}