package org.example.toJson;

public class PrivateChatJson {

    private int _id;
    private String username;
    private String email;
    private boolean isOnline;
    private String avatar;

    public PrivateChatJson(int _id, String username, String email, boolean isOnline, String avatar) {
        this._id = _id;
        this.username = username;
        this.email = email;
        this.isOnline = isOnline;
        this.avatar = avatar;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "PrivateChatJson{" +
                "_id=" + _id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", isOnline=" + isOnline +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
//"_id": "5a5b8d5bbc6412151c709cff",
//        "username": "admin",
//        "email": "admin@admin.fr",
//        "isOnline": true,
//        "avatar": "https://material-ui.com/static/images/avatar/1.jpg"