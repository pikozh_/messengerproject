package org.example.toJson;

import java.util.List;

public class RoomsJson {

    private boolean is_private;
    private int _id ;
    private String name;
    private List messages;
    private String created_at;
    private String updated_at;
    private List users;

    public RoomsJson() {
    }

    public RoomsJson( boolean is_private, int _id, String name, List messages, String created_at, String updated_at, List users) {
        this.is_private = is_private;
        this._id = _id;
        this.name = name;
        this.messages = messages;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.users = users;
    }


    public boolean getIs_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getMessages() {
        return messages;
    }

    public void setMessages(List messages) {
        this.messages = messages;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public List getUsers() {
        return users;
    }

    public void setUsers(List users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "RoomsJson{" +

                ", is_private='" + is_private + '\'' +
                ", _id=" + _id +
                ", name='" + name + '\'' +
                ", messages=" + messages +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", users=" + users +
                '}';
    }
//    "is_user": true,
//            "is_private": false,
//            "_id": "5a68817e51d66f1bf3df9889",
//            "name": "5a68817e51d66f1bf3df9889",
//            "messages": [],
//            "created_at": "2018-10-01T12:00:52.176Z",
//            "updated_at": "2018-10-01T12:00:52.177Z",
//            "__v": 0,
//            "users": []
}
